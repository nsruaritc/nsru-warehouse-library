<?php
namespace Nsru\Warehouse;

trait WarehouseHelper
{
    public function getRestCollection($url) {
        $ch = curl_init();
        curl_setopt($ch, \CURLOPT_URL,               $url);
        curl_setopt($ch, \CURLOPT_RETURNTRANSFER,    1);
        curl_setopt($ch, \CURLOPT_SSL_VERIFYHOST,    false);
        curl_setopt($ch, \CURLOPT_SSL_VERIFYPEER,    false);

        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Authorization: Bearer ".$this->bearerToken;
        curl_setopt($ch, \CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            curl_close ($ch);
            return [];
        } else {
            curl_close ($ch);
            return \json_decode($result, true)['data'] ?? [];
        }
    }
}