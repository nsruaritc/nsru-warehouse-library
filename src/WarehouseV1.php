<?php
namespace Nsru\Warehouse;

class WarehouseV1
{
    use WarehouseHelper;

    private $apiEndpoint = "https://warehouse.nsru.ac.th/api/";
    private $bearerToken;

    public function __construct($userAccessToken)
    {
        $this->bearerToken = $userAccessToken;
    }

    public function getUser() {
        return $this->getRestCollection( $this->apiEndpoint."user" );
    }

    public function getApplications() {
        return $this->getRestCollection( $this->apiEndpoint."v1/applications" );
    }

    public function getApplicationPeople(array $params = []) {
        $params = \http_build_query($params);
        return $this->getRestCollection( $this->apiEndpoint."v1/application-people?".$params );
    }

}